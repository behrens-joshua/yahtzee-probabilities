#ifndef _YAHTZEE_TOSS_HPP
#define _YAHTZEE_TOSS_HPP

#include <string>

namespace yahtzee
{
	class toss
	{
	public:
		toss(const std::string& dices);
		toss(const toss&) = default;
		toss(toss&&) = default;
		toss& operator = (const toss&) = default;
		toss& operator = (toss&&) = default;

		int value_of(const std::size_t& index) const;
		int sum() const;
		int longest_chain() const;
		std::string distinct() const;
	private:
		int _count[6];
	};
}

#endif // _YAHTZEE_TOSS_HPP