#include <algorithm>
#include <sstream>

#include "toss.hpp"

yahtzee::toss::toss(const std::string& dices)
{
	_count[0] = (int)std::count_if(dices.begin(), dices.end(), [](char p) { return p == '1'; });
	_count[1] = (int)std::count_if(dices.begin(), dices.end(), [](char p) { return p == '2'; });
	_count[2] = (int)std::count_if(dices.begin(), dices.end(), [](char p) { return p == '3'; });
	_count[3] = (int)std::count_if(dices.begin(), dices.end(), [](char p) { return p == '4'; });
	_count[4] = (int)std::count_if(dices.begin(), dices.end(), [](char p) { return p == '5'; });
	_count[5] = (int)std::count_if(dices.begin(), dices.end(), [](char p) { return p == '6'; });

	// TODO improve exception
	// there are unwanted chars in it
	if (sum() > dices.size())
	{
		throw std::runtime_error("dices contains values not in [1-6]");
	}
}

int yahtzee::toss::value_of(const std::size_t& index) const
{

	if (0 < index && index < 7)
	{
		return _count[index - 1];
	}

	// TODO throw error
	return 0;
}

int yahtzee::toss::sum() const
{
	return std::accumulate(_count, _count + 6, 0);
}

int yahtzee::toss::longest_chain() const
{
	int result = 0;

	for (int c = 0; c < 6; ++c)
	{
		auto it = std::find_if(&_count[c], &_count[6], [](int p) { return p; });
		if (it != &_count[6])
		{
			result = std::max(result, (int)std::distance(&_count[0], it));
		}
		else
		{
			break;
		}
	}

	return result;
}

std::string yahtzee::toss::distinct() const
{
	std::ostringstream out;

	char c = '1';
	for (int i = 0; i < 6; ++i, ++c)
	{
		if (_count[i])
		{
			out << c;
		}
	}

	return out.str();
}