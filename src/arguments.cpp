#include <algorithm>
#include <iostream>

#include "arguments.hpp"

yahtzee::arguments::arguments(char* args[], int argc)
{
	std::copy(args, &args[argc], std::back_inserter(_elements));
}

yahtzee::arguments::arguments(std::initializer_list<std::string>& args) : _elements(args)
{
}

yahtzee::arguments::iterator yahtzee::arguments::begin() const
{
	return _elements.begin();
}

yahtzee::arguments::iterator yahtzee::arguments::end() const
{
	return _elements.end();
}

yahtzee::arguments::iterator yahtzee::arguments::find_small(const yahtzee::arguments::value_type& key) const
{
	yahtzee::arguments::value_type test = "-" + key + "=";
	return std::find_if(begin(),
	                    end(),
	                    [test](yahtzee::arguments::value_type p)
	                    {
		                    return test.find(p) == 0;
	                    });
}

yahtzee::arguments::iterator yahtzee::arguments::find_big(const yahtzee::arguments::value_type& key) const
{
	yahtzee::arguments::value_type test = "--" + key + "=";
	return std::find_if(begin(),
	                    end(),
	                    [test](yahtzee::arguments::value_type p)
	                    {
		                    return test.find(p) == 0;
	                    });
}

yahtzee::arguments::iterator yahtzee::arguments::find(const yahtzee::arguments::value_type& small_key, const yahtzee::arguments::value_type& big_key) const
{
	yahtzee::arguments::value_type small_test = "-" + small_key + "=";
	yahtzee::arguments::value_type big_test = "--" + big_key + "=";
	return std::find_if(begin(),
	                    end(),
	                    [small_test, big_test](yahtzee::arguments::value_type p)
	                    {
		                    return p.find(big_test) == 0 || p.find(small_test) == 0;
	                    });
}

bool yahtzee::arguments::is_key_value(const yahtzee::arguments::iterator& entry) const
{
	yahtzee::arguments::value_type::size_type pos = entry->find('=');
	return (yahtzee::arguments::value_type::npos != pos) && (pos < (entry->size() - 1));
}

yahtzee::arguments::pair_type yahtzee::arguments::get_key_value(const yahtzee::arguments::iterator& entry) const
{
	if (is_key_value(entry))
	{
		yahtzee::arguments::value_type::size_type pos = entry->find('=');
		return std::make_pair(entry->substr(0, pos), entry->substr(pos + 1));
	}

	// TODO improve exception
	throw std::runtime_error("item is no key value pair");
}

std::size_t yahtzee::arguments::index_of(const yahtzee::arguments::iterator& entry) const
{
	return (std::size_t)std::distance(begin(), entry);
}

yahtzee::arguments::iterator yahtzee::arguments::get_item_at(const std::size_t& index) const
{
	if (index < size())
	{
		yahtzee::arguments::iterator it = begin();
		std::advance(it, index);
		return it;
	}

	return end();
}

std::size_t yahtzee::arguments::index_of(const yahtzee::arguments::pair_type& entry) const
{
	yahtzee::arguments::value_type test = entry.first + '=' + entry.second;
	return index_of(std::find_if(begin(),
	                             end(),
	                             [test](yahtzee::arguments::value_type p)
	                             {
		                             return test == p;
	                             }));
}

yahtzee::arguments::pair_type yahtzee::arguments::get_key_value_at(const std::size_t& index) const
{
	if (index < size())
	{
		yahtzee::arguments::iterator it = begin();
		std::advance(it, index);
		return get_key_value(it);
	}

	return std::make_pair(nullptr, nullptr);
}

std::size_t yahtzee::arguments::size() const
{
	return _elements.size();
}
