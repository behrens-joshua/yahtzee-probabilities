#include <iostream>

#include "arguments.hpp"
#include "toss.hpp"

int main(int argc, char** args)
{
	try
	{
		yahtzee::arguments arguments(args, argc);

		auto givenFaces = arguments.find("f", "faces");

		if (givenFaces != arguments.end())
		{
			std::string faces = arguments.get_key_value(givenFaces).second;

			std::cout << "Faces given: " << faces << std::endl;

			yahtzee::toss toss(faces);
			std::cout << "Stats:" << std::endl;
			for (std::size_t c = 1; c < 7; ++c)
				std::cout << '\t' << c << ":\t" << toss.value_of(c) << std::endl;
			std::cout << "Street:\t" << toss.longest_chain() << std::endl;
			std::cout << "Numbers:\t" << toss.distinct() << std::endl;
		}
		else
		{
			std::cout << "No faces given." << std::endl;
		}
	}
	catch (std::exception const& ex)
	{
		std::cout << "An error occured: " << ex.what() << std::endl;
		return 1;
	}

	return 0;
}