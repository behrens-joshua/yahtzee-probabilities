#ifndef _YAHTZEE_ARGUMENTS_HPP
#define _YAHTZEE_ARGUMENTS_HPP

#include <string>
#include <initializer_list>
#include <list>

namespace yahtzee
{
	class arguments
	{
	public:
		arguments(char* args[], int argc);
		arguments(std::initializer_list<std::string>& args);

		arguments(const arguments&) = default;
		arguments(arguments&&) = default;
		arguments& operator = (const arguments&) = default;
		arguments& operator = (arguments&&) = default;

		typedef std::string value_type;
		typedef std::list<value_type>::const_iterator const_iterator;
		typedef const_iterator iterator;
		typedef std::pair<value_type, value_type> pair_type;

		iterator begin() const;
		iterator end() const;
		iterator find_small(const value_type& key) const;
		iterator find_big(const value_type& key) const;
		iterator find(const value_type& small_key, const value_type& big_key) const;

		bool is_key_value(const iterator& entry) const;
		pair_type get_key_value(const iterator& entry) const;

		std::size_t index_of(const iterator& entry) const;
		iterator get_item_at(const std::size_t& index) const;

		std::size_t index_of(const pair_type& entry) const;
		pair_type get_key_value_at(const std::size_t& index) const;

		std::size_t size() const;
	private:
		std::list<std::string> _elements;
	};
}

#endif // _YAHTZEE_ARGUMENTS_HPP